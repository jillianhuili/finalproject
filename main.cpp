/*
--------------FROGGER GAME-----------------
By: Huili Chen and Jillian Montalvo
Created: 3/23/2013
CSE 20212 Fundamentals in Computing II

This program is our attempt to replicate the popular computer game Frogger.
The goal of the game is for the user to navigate the frog through the street
without hitting any of the oncomming traffic. Then Frogger must brave the
river where he must leap from lily pads to logs in order to rescue his frog
friends on the other side.
Frogger has three attempts to save all five of his friends as fast as possible
without hitting a car, landing in the river or running out of time. Good Luck!
-------------------------------------------
*/

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"
#include "frog.h"
#include "Car.h"
#include "log.h"
#include "lilipad.h"
#include "saveFrog.h"
#include "object.h"
#include "objectControl.h"
#include "typeFonts.h"
#include "Button.h"
#include<deque>
#include<algorithm>
#include<iterator>
#include <string>
#include <stdio.h>
#include <iostream>
using namespace std;

//define HEIGHT, WIDTH, DELAY as consts
const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 700;
const int SCREEN_BPP = 32;

//the surfaces that will be used
SDL_Surface *screen = NULL;

//holds picture that refreshes screen
SDL_Surface *background=NULL;

//The event structure that will be used
SDL_Event event;

//initializes SDL
bool init()
{
        //Initialize all SDL subsystems
        if(SDL_Init(SDL_INIT_EVERYTHING)==-1) return false;

        //Set up the screen
        screen = SDL_SetVideoMode(SCREEN_WIDTH,SCREEN_HEIGHT,SCREEN_BPP,SDL_SWSURFACE);

        //If there was an error in setting up the system
        if(screen == NULL) return false;

	//Initialize SDL_ttf
	if(TTF_Init() == -1 ) return false;

        //Set the window Caption
        SDL_WM_SetCaption("Frogger",NULL);

        //If everything initialialized as fine
        return true;
}


int main(int argc, char* args[])
{
        //make sure program waits for a quit
        bool quit = false;

	//if the game is over, igame is 1
	int igame=0;
	//if the player restarts the game, restart is 1
	int restart=0;
	int over1=0;

	//the total score of the game
	int totalScore=0;

	//the number of frogs that have been saved
	int savedFrog=0;

	//number of frog's lives
	int numLife=3;

	//"replay button's position
	int replay_x=SCREEN_WIDTH/2-100;
	int replay_y=SCREEN_HEIGHT-200;

	//set mouseDetection
	Button myButton( replay_x-30, replay_y-20, 200, 80);
	myButton.set_events(&event);
	Button gameOver1(SCREEN_WIDTH/4,SCREEN_HEIGHT/2-40,200,80);
	gameOver1.set_events(&event);

	//Load background picture
	background = SDL_LoadBMP( "background.bmp");
	if(background == NULL) cout << "Could not load background" << endl;

        //Initialize SDL
        if(init()==false) return 1;

	//display "score" on the screen
	string ttfType="Gabriola.ttf";
	typeFonts font1(ttfType,screen,35);
	font1.setContents("Score: ");
	font1.setPosition(25,SCREEN_HEIGHT-50);
	font1.setColor(152,251,152); ///green
	font1.print();
	typeFonts score(ttfType,screen,35);
	score.setPosition(150,SCREEN_HEIGHT-50);
	score.setColor(152,251,152);

	//display the number of the frog's lives
	typeFonts frog_life(ttfType,screen,35);
	frog_life.setContents("Life: ");
	frog_life.setPosition(200,SCREEN_HEIGHT-50);
	frog_life.setColor(152,251,152);
	frog_life.print();
	typeFonts frog_life2(ttfType,screen,35);
	frog_life2.setPosition(300,SCREEN_HEIGHT-50);
	frog_life2.setColor(152,251,152);

	//display "time" on the screen.
	typeFonts time(ttfType,screen,35);
	int remainingTime=200;
	time.setContents("Time: ");
	time.setPosition(SCREEN_WIDTH/2,SCREEN_HEIGHT-50);
	time.setColor(152,230,152);
	time.print();
	typeFonts timeValue(ttfType,screen,35);
	timeValue.setPosition(SCREEN_WIDTH/2+100,SCREEN_HEIGHT-50);
	timeValue.setColor(152,230,152);
	timeValue.displayNumber(remainingTime);

	//"Game Over", text that is displayed after the game is over
	typeFonts gameOver(ttfType,screen,60);
	gameOver.setContents("Game Over");
	gameOver.setPosition(SCREEN_WIDTH/4+100,SCREEN_HEIGHT/2);
	gameOver.setColor(0,0,0);

	//"Replay", text that is displayed after the game is over
	typeFonts replay(ttfType,screen,60);
	replay.setContents("Replay");
	replay.setPosition(replay_x,replay_y);
	replay.setColor(0,0,0);

	//"You win",text that is displayed after the game is over
	typeFonts win(ttfType,screen,60);
	win.setContents("You Win");
	win.setPosition(SCREEN_WIDTH/4+100,SCREEN_HEIGHT/2);
	win.setColor(0,0,0);

	//Initialize and print Frogger
        frog Frogger(screen);
        Frogger.print();

	//draw logs and liliy pads on the screen
   	objectControl<log> ilog(screen,50,SCREEN_WIDTH,SCREEN_HEIGHT,2);//
	objectControl<lilipad> ililipad(screen,100,SCREEN_WIDTH,SCREEN_HEIGHT,1);
	objectControl<log> ilog2(screen,150,SCREEN_WIDTH,SCREEN_HEIGHT,2);//
	objectControl<log> ilog3(screen,200,SCREEN_WIDTH,SCREEN_HEIGHT,1);
	objectControl<lilipad> ililipad2(screen,250,SCREEN_WIDTH,SCREEN_HEIGHT,2);//

   	//draw cars on the screen
   	objectControl<Car> icar(screen,350,SCREEN_WIDTH,SCREEN_HEIGHT,1);
	objectControl<Car> icar2(screen,400,SCREEN_WIDTH,SCREEN_HEIGHT,2);//
	objectControl<Car> icar3(screen,450,SCREEN_WIDTH,SCREEN_HEIGHT,1);
	objectControl<Car> icar4(screen,500,SCREEN_WIDTH,SCREEN_HEIGHT,2);//
	objectControl<Car> icar5(screen,550,SCREEN_WIDTH,SCREEN_HEIGHT,1);

	//draw Frogs to be saved on screen
	saveFrog frog1(screen,75,0,1);
	saveFrog frog2(screen,225,0,2);
	saveFrog frog3(screen,375,0,3);
	saveFrog frog4(screen,525,0,4);
	saveFrog frog5(screen,675,0,5);


	//set timer
	//the timer starting time
	Uint32 startTime = 0;
	Uint32 runningTime=0;

	//start the timer
	startTime=SDL_GetTicks();

        //While the user hasn't quit
        while(quit==false){

		//initialize the frog's location
		if(restart==1){
	    		Frogger.resetLocation(&startTime,&runningTime);
			totalScore=0;
			frog1.regenerateTheFrog();
			frog2.regenerateTheFrog();
			frog3.regenerateTheFrog();
			frog4.regenerateTheFrog();
			frog5.regenerateTheFrog();
			savedFrog = 0;
	    	}
	    	restart=0;
	    	over1=0;
	    	if(igame==0){
			//add background screen on the top of the screen buffer
			SDL_BlitSurface(background, NULL, screen, NULL);

			//get current time
			runningTime=SDL_GetTicks()-startTime;

			//move logs, lily pads and logs on the screen.
			ilog.move(runningTime);
			ilog2.move(runningTime);
			ilog3.move(runningTime);
			icar.move(runningTime);
			icar2.move(runningTime);
			icar3.move(runningTime);
			icar4.move(runningTime);
			icar5.move(runningTime);
			ililipad.move(runningTime);
			ililipad2.move(runningTime);

			//move Frogger on screen
			Frogger.move();

			//the time is up, then quit the game.
			if(remainingTime<=0&&numLife==1){
				Frogger.Crash();
				igame=1;
			}else if(remainingTime<=0&&numLife>1){
				Frogger.Crash();	
				numLife=numLife-1;
				Frogger.resetLocation(&startTime,&runningTime);
			}

			//check if Frogger collides with car. If the frog only has one life,
			//then the game is over. If the frog has more than one lives, the game continues;
			if(Frogger.checkCollision(icar)&&numLife==1) {
				Frogger.Crash();
		         	igame=1;
			}else if(Frogger.checkCollision(icar)&&numLife>1){
				Frogger.Crash();
				numLife=numLife-1;
				Frogger.resetLocation(&startTime,&runningTime);
			}
			if(Frogger.checkCollision(icar2)&&numLife==1){
				Frogger.Crash();
				igame=1;
			}else if(Frogger.checkCollision(icar2)&&numLife>1){
				Frogger.Crash();
				numLife=numLife-1;
				Frogger.resetLocation(&startTime,&runningTime);
			}
			if(Frogger.checkCollision(icar3)&&numLife==1) {
				Frogger.Crash();
		         	igame=1;
			}else if(Frogger.checkCollision(icar3)&&numLife>1){
				Frogger.Crash();
				numLife=numLife-1;
				Frogger.resetLocation(&startTime,&runningTime);
			}
			if(Frogger.checkCollision(icar4)&&numLife==1) {
				Frogger.Crash();
		         	igame=1;
			}else if(Frogger.checkCollision(icar4)&&numLife>1){
				Frogger.Crash();
				numLife=numLife-1;
				Frogger.resetLocation(&startTime,&runningTime);
			}
			if(Frogger.checkCollision(icar5)&&numLife==1){
				Frogger.Crash();
		         	igame=1;
			}else if(Frogger.checkCollision(icar5)&&numLife>1){
				Frogger.Crash();
				numLife=numLife-1;
				Frogger.resetLocation(&startTime,&runningTime);
			}

			//check if Frogger jumps on log or lily pad
			if(Frogger.getYlocation()==ilog.get_y_position(0)&&Frogger.checkCollision(ilog)&&Frogger.getYlocation()<=250){
				Frogger.Drown();
				if(numLife>1){
					numLife=numLife-1;
					Frogger.resetLocation(&startTime,&runningTime);
				}else{
				 	igame=1;
				}
			}
			if(Frogger.getYlocation()==ilog2.get_y_position(0)&&Frogger.checkCollision(ilog2)&&Frogger.getYlocation()<=250){
				Frogger.Drown();
				if(numLife>1){
					numLife=numLife-1;
					Frogger.resetLocation(&startTime,&runningTime);
				}else{
				 	igame=1;
				}
			}
			if(Frogger.getYlocation()==ilog3.get_y_position(0)&&Frogger.checkCollision(ilog3)&&Frogger.getYlocation()<=250){
				Frogger.Drown();
				if(numLife>1){
					numLife=numLife-1;
					Frogger.resetLocation(&startTime,&runningTime);
				}else{
				 	igame=1;
				}
			}
			if(Frogger.getYlocation()==ililipad.get_y_position(0)&&Frogger.checkCollision(ililipad)&&Frogger.getYlocation()<=250){
				Frogger.Drown();
				if(numLife>1){
					numLife=numLife-1;
					Frogger.resetLocation(&startTime,&runningTime);
				}else{
				 	igame=1;
				}
			}
			if(Frogger.getYlocation()==ililipad2.get_y_position(0)&&Frogger.checkCollision(ililipad2)&&Frogger.getYlocation()<=250){
				Frogger.Drown();
				if(numLife>1){
					numLife=numLife-1;
					Frogger.resetLocation(&startTime,&runningTime);
				}else{
				 	igame=1;
				}
			}

			//check if Frogger saves frog
			if(frog1.AlreadySaved()==0&&frog1.SaveTheFrog(Frogger.getLocation())){
				Frogger.resetLocation(&startTime,&runningTime); //saves frog and resets to original position
				totalScore=50+remainingTime*10+totalScore; //calculate new Score
				savedFrog+=1;
			}else if(frog2.AlreadySaved()==0&&frog2.SaveTheFrog(Frogger.getLocation())){
				Frogger.resetLocation(&startTime,&runningTime); //saves frog and resets to original position
				totalScore=50+remainingTime*10+totalScore; //calculate new Score
				savedFrog+=1;			
			}else if(frog3.AlreadySaved()==0&&frog3.SaveTheFrog(Frogger.getLocation())){
				Frogger.resetLocation(&startTime,&runningTime); //saves frog and resets to original position
				totalScore=50+remainingTime*10+totalScore; //calculate new Score
				savedFrog+=1;
			}else if(frog4.AlreadySaved()==0&&frog4.SaveTheFrog(Frogger.getLocation())){ 
				Frogger.resetLocation(&startTime,&runningTime); //saves frog and resets to original position
				totalScore=50+remainingTime*10+totalScore; //calculate new Score
				savedFrog+=1;
			}else if(frog5.AlreadySaved()==0&&frog5.SaveTheFrog(Frogger.getLocation())){
				Frogger.resetLocation(&startTime,&runningTime); //saves frog and resets to original position
				totalScore=50+remainingTime*10+totalScore; //calculate new Score
				savedFrog+=1;
			//or if frog has already been saved
			}else if(frog1.AlreadySaved()==1&&frog1.SaveTheFrog(Frogger.getLocation())){
				//do nothing
			}else if(frog2.AlreadySaved()==1&&frog2.SaveTheFrog(Frogger.getLocation())){
				//do nothing
			}else if(frog3.AlreadySaved()==1&&frog3.SaveTheFrog(Frogger.getLocation())){
				//do nothing
			}else if(frog4.AlreadySaved()==1&&frog4.SaveTheFrog(Frogger.getLocation())){
				//do nothing
			}else if(frog5.AlreadySaved()==1&&frog5.SaveTheFrog(Frogger.getLocation())){
				//do nothing
			//or if frog ends up in water and gameover
			}else if(!frog1.SaveTheFrog(Frogger.getLocation())&&Frogger.getYlocation()==0){
				//Frogger dies
				Frogger.Drown();
				if(numLife==1){
					igame = 1;
				}else{
					numLife=numLife-1;
					Frogger.resetLocation(&startTime,&runningTime);
				}
			}else if(!frog2.SaveTheFrog(Frogger.getLocation())&&Frogger.getYlocation()==0){
				//Frogger dies
				Frogger.Drown();
				if(numLife==1){
					igame = 1;
				}else{
					numLife=numLife-1;
					Frogger.resetLocation(&startTime,&runningTime);
				}
			}else if(!frog3.SaveTheFrog(Frogger.getLocation())&&Frogger.getYlocation()==0){
				//Frogger dies
				Frogger.Drown();
				if(numLife==1){
					igame = 1;
				}else{
					numLife=numLife-1;
					Frogger.resetLocation(&startTime,&runningTime);
				}
			}else if(!frog4.SaveTheFrog(Frogger.getLocation())&&Frogger.getYlocation()==0){
				//Frogger dies
				Frogger.Drown();
				if(numLife==1){
					igame = 1;
				}else{
					numLife=numLife-1;
					Frogger.resetLocation(&startTime,&runningTime);
				}
			}else if(!frog5.SaveTheFrog(Frogger.getLocation())&&Frogger.getYlocation()==0){
				//Frogger dies
				Frogger.Drown();
				if(numLife==1){
					igame = 1;
				}else{
					numLife=numLife-1;
					Frogger.resetLocation(&startTime,&runningTime);
				}
			}
			if(savedFrog==5) igame = 1;
			//display score on the screen
			font1.print();
			score.displayNumber(totalScore);

			//display the number of the frog's lives left
			frog_life.print();
			frog_life2.displayNumber(numLife);

			//display time on the screen
			remainingTime=25-runningTime/1000;
			time.print();
			timeValue.displayNumber(remainingTime);

			//print frog
			Frogger.print();

		//when the game is over
		}else if(igame==1&&savedFrog<5){
			SDL_BlitSurface(background, NULL, screen, NULL);
			gameOver.setContents("Game Over");
			gameOver.print();
			replay.setContents("Replay");
			replay.print();
			font1.print();
			score.displayNumber(totalScore);

		//the user wins the game
 		}else{
			SDL_BlitSurface(background, NULL, screen, NULL);
			//cout<<"You win!"<<endl;
			win.setContents("You Win!");
			win.print();
			replay.setContents("Replay");
			replay.print();
			font1.print();
			score.displayNumber(totalScore);
		}

		//While there's an event to handle
		while(SDL_PollEvent(&event)){
			//Handle button events
			if(igame==1){
				myButton.handle_events();
				restart=myButton.mouse_over_position();
				gameOver1.handle_events();
				over1=gameOver1.mouse_over_position();
			}

	                //If the user has xed out of the window
	                if(event.type==SDL_QUIT||over1==1){
        	                //Quit the program
				quit = true;
				break;
                	}
	                //Get keystates
	                Uint8 *keystates = SDL_GetKeyState(NULL);
			if(keystates[SDLK_UP]&&igame==0){
                        	Frogger.up();
                	}
                	if(keystates[SDLK_DOWN]&&igame==0){
                        	Frogger.down();
                	}
                	if(keystates[SDLK_LEFT]&&igame==0){
                        	Frogger.left();
                	}
                	if(keystates[SDLK_RIGHT]&&igame==0){
                        	Frogger.right();
                	}
                	if(keystates[SDLK_END] || keystates[SDLK_PAGEUP]){
				//END or PAGEUP buttons quit the game
                        	quit = true;
                        	break;
                	}
                	if(SDL_Flip(screen)==-1) return 1;
		}

		//Update the screen
        	if(SDL_Flip(screen)==-1) return 1;

		//if the user restarts the game, then rerun the game
		if(restart==1){
			igame=0;
			numLife=3;
			myButton.resetMouseDetection();
			continue;
		}
        }

	//Free all surfaces
	//SDL_FreeSurface(background);
	//SDL_FreeSurface(screen);
	//ilog.free_surfaces();
	//ilog2.free_surfaces();
	//ilog3.free_surfaces();
    	//icar.free_surfaces();
	//icar2.free_surfaces();
	//icar3.free_surfaces();
	//icar4.free_surfaces();
	//icar5.free_surfaces();
	//ililipad.free_surfaces();
	//ililipad2.free_surfaces();
	//win.free();
	//font1.free();
	//score.free();
	//frog_life.free();
	//frog_life2.free();
	//time.free();
	//timeValue.free();
	//gameOver.free();
	//replay.free();


   	//Quit SDL_ttf
	//TTF_Quit();
	
	//Quit SDL
        SDL_Quit();
        return 0;
}




