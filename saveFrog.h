/*
saveFrog.h
contains the definition for the saveFrog class
By: Jillian Montalvo
*/

#ifndef SAVE_FROG_H
#define SAVE_FROG_H
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "frog.h"
#include "Car.h"
#include "log.h"
#include "lilipad.h"
#include "object.h"
#include "objectControl.h"
#include <deque>
#include <algorithm>
#include <string>
#include <iostream>
using namespace std;

class saveFrog{
	public:
		saveFrog(SDL_Surface*,int,int,int); //constructor with location inputs
		~saveFrog(); //deconstructor
		void print(); //prints frog only if frog has not been saved
		int getXlocation(); //returns x
		int getYlocation(); //returns y	
		bool SaveTheFrog(SDL_Rect); //runs checkCollision to see if frog has been saved
		bool checkCollision(SDL_Rect); //checks for collision between Frogger and frog
		int AlreadySaved(); //returns value of saved in order to tell if frog was alreayd saved
		void regenerateTheFrog();
	private:
		object obj; //object contains necessary printing functions to put image on screen
		SDL_Surface *Frog; //will be image displayed
		SDL_Surface *screen; //holds screen pointer
		SDL_Rect location; //contains the location of the class
		int saved; //if 0 frog is not saved, if 1 frog has been saved
};

#endif
