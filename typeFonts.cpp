//
//  typeFonts.cpp
//
//
//  Created by Huili Chen on 4/5/13.
// 
//
#include <iostream>
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"
#include "typeFonts.h"
#include "object.h"
#include <string>
#include <sstream>
#include <stdio.h>
#include <cstring>
using namespace std;

typeFonts::typeFonts(string filename, SDL_Surface *screen,int size):obj(screen){
	
	//
	iscreen=screen;
	
	//the font that is going to be used
	font=NULL;

	//the color of the font
 	//SDL_Color white={255,255,255};
	//textColor=white;
	const char *m=&filename.at(0);
	font=TTF_OpenFont(m,size);
	
	if(font==NULL){
		cout<<"Could not load the file!"<<endl;
	}

}

void typeFonts::setContents(string text){	
	contents=&text.at(0);
	

}



void typeFonts::print(){

	//Render the text
    message = TTF_RenderText_Solid( font, contents, textColor );
	//If there was an error in rendering the text
    if( message == NULL )
    {
        cout<<"message is null!"<<endl;   
    }
    //Apply the images to the screen
    obj.print(ix,iy,message);
    

}

void typeFonts::free(){
	//Free the surfaces
//    SDL_FreeSurface( message );
    
    //Close the font that was used
    TTF_CloseFont( font );
    

}

void typeFonts::setPosition(int x,int y){
	ix=x;
	iy=y;

}

void typeFonts::setColor(int c1,int c2,int c3){

	//the color of the font
	static const SDL_Color c={c1,c2,c3};
	textColor=c;
}

void typeFonts::displayNumber(int num){
	char *result;
	std::stringstream convert;
	convert<< num;
	result=&convert.str().at(0);
	message = TTF_RenderText_Solid( font, result, textColor );
	//If there was an error in rendering the text
    	if( message == NULL )
    	{
     	   cout<<"number is null!"<<endl;   
   	 }
   	 //Apply the images to the screen
   	 obj.print(ix,iy,message);
}
