//
//  typeFonts.h
//
//
//  Created by Huili Chen on 4/5/13.
// 
//
#ifndef typeFonts_h
#define typeFonts_h

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"
#include "object.h"
#include <string>
#include <iostream>
#include <stdio.h>
#include <cstring>
using namespace std;

class typeFonts{
	public:
		typeFonts(string, SDL_Surface *,int); //constructor
		void print(); //print function
		void free(); //frees image
		void setContents(string );
		void setPosition(int,int);
		void setColor(int,int,int);
		void displayNumber(int );
	private:
		object obj;//object contains necessary printing functions to put image on screen
		SDL_Surface *iscreen;
		SDL_Surface *message;//create a surface for message
		SDL_Color textColor;//the color of the font
		const char *contents;//set contents
		TTF_Font *font;//the font that is going to be used
		int ix; //x position of the message
		int iy; //y position of the message
};

#endif
