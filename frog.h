/*
frog.h
contains the definition for the frog class
By: Jillian Montalvo
*/

#ifndef FROG_H
#define FROG_H

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "Car.h"
#include "log.h"
#include "lilipad.h"
#include "saveFrog.h"
#include "object.h"
#include "objectControl.h"
#include <deque>
#include <algorithm>
#include <stdio.h>
#include <string>
#include <iostream>
using namespace std;

class frog{
	public:
		frog(SDL_Surface*); //constructor with location inputs
		~frog(); //deconstructor
		int getXlocation(); //returns x
		int getYlocation(); //returns y	
		SDL_Rect getLocation(); //returns location as SDL_Rect
		void print(); //prints frog
		void left(); //moves left
		void right(); //moves right
		void up(); //moves up
		void down(); //moves down
		void move();
		void Crash();
		void Drown();
		void resetLocation(Uint32 *,Uint32 *);//returns frog to start location
		/*collision detection functions*/
		bool checkCollision(objectControl<Car>); 
		bool checkCollision(objectControl<log>);
		bool checkCollision(objectControl<lilipad>);
	private:
		object obj; //object contains necessary printing functions to put image on screen
		int dx; //x velocity
		SDL_Rect location; //contains location of frog
		SDL_Surface *Frog; //will be image displayed
		deque <SDL_Surface*> upFrog; //holds up frog pictures
		deque <SDL_Surface*> downFrog; //holds down frog pictures
		deque <SDL_Surface*> leftFrog; //holds left frog pictures
		deque <SDL_Surface*> rightFrog; //holds right frog pictures
		SDL_Surface *screen; //holds screen pointer
		SDL_Surface *splash;
		SDL_Surface *crash;
		void load_files(); //loads all pictures for frog	
};

#endif
