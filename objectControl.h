//
//  objectControl.h
//	This class is used to create deques of either logs, cars, or
//	lily pads, and then move them across the screen
//  Created by Huili Chen on 3/25/13.
//  
//

#ifndef objectControl_h
#define objectControl_h
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include <deque>
#include <algorithm>
#include <iterator>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
using namespace std;

template<typename T>   //templated class
class objectControl{
	public:
    		objectControl(SDL_Surface*,int,int,int,int); //constructor  	 	
		int get_x_position(int); //returns x position of number of element in data
    		int get_y_position(int); //returns y position of number of element in data
		int getVelocity(); //returns the velocity
    		int get_num_of_objects(); //returns the size of data
		T getObject(int); //returns the object contained in data
		void move(Uint32); //moves the objects in data across the screen
		void print(); //prints the objects on the screen
    		//void increse_speed();
    		void free_surfaces();
	private:
    		std::deque<T> data; //contains the objects of type T 
    		int num; //number of objects in data
    		int dx; //x velocity
    		int xBoundary; //width of the SDL window
    		int yBoundary; //height of the SDL window
    		int yPosition; //y position of the objects in data
		int dir; // if 1 move right to left, if 2 move left to right
		SDL_Surface* iscreen; //pointer to the SDL window
    		
};

//constructor
template<typename T>
objectControl<T>::objectControl(SDL_Surface* screen,int yP, int xB,int yB,int direction)
{		
	//determine a random velocity
	dx = rand()%8+3;
	
	//if the direction is 1 <--
	//if the direction is 2 -->
	dir=direction;
	if(dir==2){
		dx=-dx;
	}
	
	//set other variables
	xBoundary=xB;
	yBoundary=yB;
	yPosition=yP;

	//set screen pointer
	iscreen=screen;

	//fill in data
	num=10;
	if(dir==1){ // <--
		//the new object will be based on the previous object's
		//xLocation+width because that gives us the right side
		//of the object we want to follow. Then we add on a random
		//number to give us the distance between the two objects
		for(int i=0;i<num;i++){
       			T object1(screen); //new object
			int width;
			int xLocation;

			//if it is the first object then we don't want to call data[i-1]
			if(i!=0){
				//sets width and location of prior object
				width = data[data.size()-1].getWidth();
				xLocation = data[data.size()-1].get_x_position();
			}else{
				width = 0;
				xLocation = 1;
			}
			//set position and velocity and add to data			
			object1.setPosition(width+xLocation+10*(rand()%30+1),yPosition);
			object1.setVelocity(dx);
			data.push_back(object1);
		}
	}else{ // -->
		//Now, the new object will be based off of the xLocation of the
		//previous minus the width of the new object and minus the space
		//between them
		for(int i=0;i<num;i++){
       			T object1(screen); //new object
			int width;
			int xLocation;
			
			//if it is the first object then we don't want to call data[i-1]
			if(i!=0){
				//sets width of new object and xLocation of prior object
				width = object1.getWidth();
				xLocation = data[data.size()-1].get_x_position();
			}else{
				width = 0;
				xLocation = 799;
			}
			//set position and velocity and add to data
			object1.setPosition(xLocation-width-10*(rand()%30+1),yPosition);
			object1.setVelocity(dx);
			data.push_back(object1);
		}
	}
	print();
}

//Prints all the objects in data to screen
template<typename T>
void objectControl<T>::print()
{	 
	for(int i=0;i<data.size();i++){
		data[i].put_image_on_screen();        
	}
}

//Moves the objects in data across the screen
template<typename T>
void objectControl<T>::move(Uint32 times)
{
	int newX;
	int newY;

	//object1 is for the deque of cars that are moving from the right to the left <--
	T object1(iscreen);
	object1.setPosition(data[data.size()-1].get_x_position()+data[data.size()-1].getWidth()+10*(rand()%30+1),yPosition);
	object1.setVelocity(dx);

	//object2 is for the deque of cars taht are moving from the left to the right -->
	T object2(iscreen);
	object2.setPosition(data[data.size()-1].get_x_position()-object2.getWidth()-10*(rand()%30+1),yPosition);	
	object2.setVelocity(dx);	
		
		
	
	for(int i=0;i<data.size();i++){
		//calculate new positions
		newX=data[i].get_x_position()-dx;
		newY=data[i].get_y_position();

		//check whether the object's new position is in the bound. 
		//If not, it will be deleted and a new object will be created.
		if(dir==1 && newX>=-300){  
			// <-- new position
			data[i].setPosition(newX,newY);
		}else if(dir==2 && newX<=xBoundary+300){ 
			// -->	new position
			data[i].setPosition(newX,newY);
		}else{
			if(dir==1){
				// <-- new object
				data.pop_front();			
				data.push_back(object1);	
			}else{
				// --> new object
				data.pop_front();
				data.push_back(object2);
			}
		}
	}
	print();
}

//returns the velocity
template<typename T>
int objectControl<T>::getVelocity(){
	return dx;
}

//Frees all of the surfaces contained in the objects in data
template<typename T>
void objectControl<T>::free_surfaces(){
	for(int i=0;i<data.size();i++){
		data[i].free_surface();
	}
}

//returns the x position of the nth object in data
template<typename T>
int objectControl<T>::get_x_position(int n){
	return data[n].get_x_position();
} 

//returns the y position of the nth object in data
template<typename T>
int objectControl<T>::get_y_position(int n){
	return data[n].get_y_position();
} 

//returns size of data
template<typename T>
int objectControl<T>::get_num_of_objects(){
	return data.size();

}

//returns the nth object in data
template<typename T>
T objectControl<T>::getObject(int n)
{
	return data[n];
}

#endif

