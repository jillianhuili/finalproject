/*
saveFrog.cpp
contains the implementation for the class saveFrog
By: Jillian Montalvo
*/
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "frog.h"
#include "Car.h"
#include "log.h"
#include "lilipad.h"
#include "saveFrog.h"
#include "object.h"
#include "objectControl.h"
#include <deque>
#include <algorithm>
#include <string>
#include <iostream>
using namespace std;

//constructor
saveFrog::saveFrog(SDL_Surface* Screen,int x,int y,int color):obj(Screen)
{
	//set location of saveFrog
	location.x = x;
	location.y = y;
	location.w = 50;
	location.h = 50;

	//load correct colored image
	if(color == 1){
		Frog = obj.load_image("SaveFrog5.png");
	}else if(color == 2){
		Frog = obj.load_image("SaveFrog4.png");
	}else if(color == 3){
		Frog = obj.load_image("SaveFrog2.png");
	}else if(color == 4){
		Frog = obj.load_image("SaveFrog1.png");
	}else if(color == 5){
		Frog = obj.load_image("SaveFrog3.png");
	}
	
	//save window pointer
	screen = Screen;

	//set save to 0 because frog is not saved
	saved = 0;
}

//deconstructor
saveFrog::~saveFrog()
{
	//free image
	SDL_FreeSurface(Frog);
}

//prints frog on screen if it has not already been saved
void saveFrog::print()
{
	if(saved == 0){
		obj.print(location.x,location.y,Frog);
	}
}

//returns x location of frog
int saveFrog::getXlocation()
{
	return location.x;
}

//returns y location of frog
int saveFrog::getYlocation()
{
	return location.y;
}

//Checks to see if frog has been saved. 
//if it has not been saved, then it prints the frog on screen
//if it has been saved, then it changes saved to 1 so it will not print on screen
bool saveFrog::SaveTheFrog(SDL_Rect Frogger)
{
	bool isSaved = false;
	if(checkCollision(Frogger)){
		saved = 1;
		isSaved = true;
	}
	print();
	return isSaved;
}

void saveFrog::regenerateTheFrog(){
	saved=0;
}

//returns value of saved to tell if frog has already been saved
//1 = saved
//0 = not saved
int saveFrog::AlreadySaved()
{
	return saved;
}

//returns true when Frogger saves frog
bool saveFrog::checkCollision(SDL_Rect Frogger)
{
	bool collision = false;

	//The sides of the rectangles 
	int leftF, leftA; 
	int rightF, rightA; 
	int topF, topA; 
	int bottomF, bottomA; 	

	//Calculate the sides of saveFrog 
	leftF = location.x+15; 
	rightF = location.x+15 + location.w-45; 
	topF = location.y; 
	bottomF = location.y + location.h-1; 	
		
	//Calculate the sides of rect Frogger 
	leftA = Frogger.x; 
	rightA = Frogger.x + Frogger.w; 
	topA = Frogger.y; 
	bottomA = Frogger.y + Frogger.h;

	//hits left side
	if(rightF>=leftA && rightF<=rightA && topF>=topA && topF<=bottomA){
		collision = true;
	} 

	//hits right side
	if(leftF<=rightA && leftF>=leftA && topF>=topA && topF<=bottomA){	
		collision = true; 		
	} 

	//hits top side
	if(bottomF>topA && bottomF<=bottomA && (rightF<=rightA+10 && rightF>=leftA-10 || leftF>=leftA+10 && leftF<=rightA-10)){ 
		collision = true;		
	}

	//hits bottom side
	if(topF<=bottomA && topF>=topA && (rightF<=rightA+10 && rightF>=leftA-10 || leftF>=leftA+10 && leftF<=rightA-10)){ 
		collision = true;
	} 	

	//If none of the sides from A are outside Frog
	return collision; 

}
