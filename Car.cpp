//
//  Car.cpp
//
//
//  Created by Huili Chen on 3/25/13.
//  
//

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "Car.h"
#include "object.h"
#include <deque>
#include <algorithm>
#include <iterator>
#include <iostream>
using namespace std;

//constructor
Car::Car(SDL_Surface* Screen):obj(Screen)
{
	//creates temporary deque of car images
    	deque<SDL_Surface*> images;

	//load images
	SDL_Surface* image1=obj.load_image("car1.png");
	SDL_Surface* image2=obj.load_image("car2.png");
	SDL_Surface* image3=obj.load_image("car3.png");
	SDL_Surface* image4=obj.load_image("car4.png");
	SDL_Surface* image6=obj.load_image("car6.png");
	SDL_Surface* image7=obj.load_image("car7.png");

	//adds images to temporary deque
	images.push_back(image1);
	images.push_back(image2);
	images.push_back(image3);
	images.push_back(image4);
	images.push_back(image6);
	images.push_back(image7);

	//selects a random number and sets that car image to car_image
	int i=rand()%images.size();
   	car_image=images[i];

	//set x and y location
    	location.x=400;
    	location.y=400;
	location.w = 50;
	location.h = 50;
}

//prints the car
void Car::put_image_on_screen()
{
    obj.print(location.x,location.y,car_image);
}

//frees the surface
void Car::free_surface()
{
    SDL_FreeSurface(car_image);
}

//sets the location of the car
void Car::setPosition(int xx,int yy)
{
    location.x=xx;
    location.y=yy;
}

//void Car::setVelocity(int x)
//{
//	x_speed=x;
//}

//returns the width of the car
int Car::getWidth()
{
	return location.w;
}

//Used to set velocity and switch the images so the car drives forward not backwards
void Car::setVelocity(int x){
	//set speed
	x_speed=x;

	//if object is traveling -->
	if(x_speed<0){
		//creates temporary deque of car images
    		deque<SDL_Surface*> images;

		//load images
		SDL_Surface* image1=obj.load_image("rcar1.png");
		SDL_Surface* image2=obj.load_image("rcar2.png");
		SDL_Surface* image3=obj.load_image("rcar3.png");
		SDL_Surface* image4=obj.load_image("rcar4.png");
		SDL_Surface* image6=obj.load_image("rcar6.png");
		SDL_Surface* image7=obj.load_image("rcar7.png");	

		//adds images to temporary deque	
		images.push_back(image1);
		images.push_back(image2);
		images.push_back(image3);
		images.push_back(image4);
		images.push_back(image6);
		images.push_back(image7);

		//selects a random number and sets that car image to car_image
		int i=rand()%images.size();
   		car_image=images[i];
	}

}

//returns the color of the car
string Car::get_color()
{
    return color;
}

//returns the x position
int Car::get_x_position()
{
    return location.x;
}

//returns the y position
int Car::get_y_position()
{
    return location.y;
}

//returns an SDL_Rect containing the location of the car
SDL_Rect Car::getLocation()
{
	return location;
}
