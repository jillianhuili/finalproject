//
//  lilipad.cpp
//
//
//  Created by Huili Chen on 4/1/13.
//  
//

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "lilipad.h"
#include "object.h"
#include <deque>
#include <algorithm>
#include <iterator>
#include <iostream>
using namespace std;

//constructor
lilipad::lilipad(SDL_Surface* Screen):obj(Screen)
{
	//creates temporary deque of lilipad images    
    	deque<SDL_Surface*> images; 

	//load images
	SDL_Surface* image1=obj.load_image("2lilipads.png");
	SDL_Surface* image2=obj.load_image("3lilipads.png");
	
	//adds images to temporary deque	
	images.push_back(image1);
	images.push_back(image2);
	

	//selects a random number and sets that lilipad image to lilipad_image
	int i=rand()%images.size();
   	lilipad_image=images[i];
 
	//set x and y location
    	location.x=400;
    	location.y=400;
	if(i==0){
		location.w = 100;
		location.h = 50;
	}else{
		location.w = 150;
		location.h = 50;
	}
}

//print function
void lilipad::put_image_on_screen()
{
    obj.print(location.x,location.y,lilipad_image);
    
}

//free surface
void lilipad::free_surface()
{
    SDL_FreeSurface(lilipad_image);
}

//set position
void lilipad::setPosition(int xx,int yy)
{
    location.x=xx;
    location.y=yy;
}

//set velocity
void lilipad::setVelocity(int x){

	x_speed=x;
}

//returns color
string lilipad::get_color()
{
    return color;
    
}

//returns width
int lilipad::getWidth()
{
	return location.w;
}

//returns x position
int lilipad::get_x_position()
{
    return location.x;
    
}

//returns y position
int lilipad::get_y_position()
{
    return location.y;
}

//returns location in form of SDL_Rect
SDL_Rect lilipad::getLocation()
{
	return location;
}
