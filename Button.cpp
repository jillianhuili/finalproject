#include <iostream>
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "Button.h"
#include<deque>
#include<algorithm>
#include<iterator>
using namespace std;


Button::Button( int x, int y, int w, int h)
{
    //Set the button's attributes
    box.x = x;
    box.y = y;
    box.w = w;
    box.h = h;
   MouseOverPosition=0;
  

}

void Button::set_events(SDL_Event *ievent)
{
  	eventPtr=ievent;
}


void Button::handle_events()
{
    //The mouse offsets
    int x = 0, y = 0;
    
    //If the mouse moved
    if( (*eventPtr).type == SDL_MOUSEMOTION )
    {
	
        //Get the mouse offsets
        x = (*eventPtr).motion.x;
        y = (*eventPtr).motion.y;
        
        //If the mouse is over the button
        if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
        {
	//	cout<<"the mouse is over the button"<<endl;
            
        }
        //If not
        else
        {
		//cout<<"the mouse is not over the button"<<endl;
           
        }    
    }
	//If a mouse button was pressed
    if( (*eventPtr).type == SDL_MOUSEBUTTONDOWN )
    {
        //If the left mouse button was pressed
        if( (*eventPtr).button.button == SDL_BUTTON_LEFT )
        {
	
            //Get the mouse offsets
            x = (*eventPtr).button.x;
            y = (*eventPtr).button.y;
      //  cout<<"mouse button down"<<endl;
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
	//cout<<"mouse button down and mouse is over the button"<<endl;
                MouseOverPosition=1;
            }
        }
    }

    //If a mouse button was released
    if( (*eventPtr).type == SDL_MOUSEBUTTONUP )
    {
        //If the left mouse button was released
        if( (*eventPtr).button.button == SDL_BUTTON_LEFT )
        { 
            //Get the mouse offsets
            x = (*eventPtr).button.x;
            y = (*eventPtr).button.y;
        
            //If the mouse is over the button
            if( ( x > box.x ) && ( x < box.x + box.w ) && ( y > box.y ) && ( y < box.y + box.h ) )
            {
	//	cout<<"mouse is released. the mouse is over the button"<<endl;
		//do something here. restart the game.
		MouseOverPosition=1;
            }
        }
    }
}

int Button::mouse_over_position(){
	return MouseOverPosition;

}

void Button::resetMouseDetection(){

	MouseOverPosition=0;
}

int Button::action()
{
    //Show the button
    //apply_surface( box.x, box.y, buttonSheet, screen, clip );
}




