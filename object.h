/*
object.h
class that contains load picture and print picture functions
by: Jillian Montalvo
*/
#ifndef OBJECT_H
#define OBJECT_H
#include "SDL/SDL.h"
#include <iostream>
using namespace std;

class object{
	public:
		object(SDL_Surface*); //constructor
		SDL_Surface* load_image(string); //loads image and turns white pixles to invisible
		void print(double,double,SDL_Surface*); //prints object to screen

	private:
		SDL_Surface* screen; //pointer to SDL window
};
#endif
