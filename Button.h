#ifndef Button_h
#define Button_h
#include <iostream>
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"

#include<deque>
#include<algorithm>
#include<iterator>
using namespace std;

//The button
class Button
{
	public:
    		//Initialize the variables	
		Button( int x, int y, int w, int h );
    		void set_events(SDL_Event *);
    
   		//Handles events and set the button's sprite region
    		void handle_events();
    		int mouse_over_position();
    		void resetMouseDetection();
    
    		//Shows the button on the screen
    		int action();
    	private:
    		//The attributes of the button
    		SDL_Rect box;
    		SDL_Event *eventPtr;
    		int MouseOverPosition;
    
    		//The part of the button sprite sheet that will be shown
   		// SDL_Rect* clip;
};

#endif
