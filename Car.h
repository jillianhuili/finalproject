//
//  Car.h
//
//
//  Created by Huili Chen on 3/25/13.
// 
//

#ifndef SDL_Template_Car_h
#define SDL_Template_Car_h
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "object.h"
#include <deque>
#include <algorithm>
#include <iterator>
#include <iostream>
using namespace std;

class Car{
    	public:
    		Car(SDL_Surface*); //constructor
        	int get_x_position(); //returns x position
        	int get_y_position(); //returns y position
		int getWidth(); //returns width of Car
		SDL_Rect getLocation(); //returns location of car in SDL_Rect
		string get_color(); //returns color of car
		void put_image_on_screen(); //print function
        	void setPosition(int,int); //sets the position of the car
		void setVelocity(int); //sets the velocity of the car
		void free_surface(); //frees the surface
	
    	private:
		object obj; //object contains necessary printing functions to put image on screen
        	int x_speed; //contains the x velocity
		int y_speed; //contains the y velocity
		string color; //contains the color of class
		SDL_Rect location; //contains the location of the class
        	SDL_Surface* car_image; //pointer to the image of the class
		SDL_Surface* screen; //pointer to the SDL window	
};
#endif
