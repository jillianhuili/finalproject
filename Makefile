all: main
main: main.o Car.o log.o frog.o lilipad.o object.o typeFonts.o saveFrog.o Button.o
	g++ main.o Car.o log.o frog.o lilipad.o object.o typeFonts.o saveFrog.o Button.o -o main -lSDL -lSDL_image -lSDL_ttf
main.o: main.cpp
	g++ -c main.cpp
Button.o: Button.cpp
	g++ -c Button.cpp
Car.o: Car.cpp
	g++ -c Car.cpp
log.o: log.cpp
	g++ -c log.cpp
frog.o: frog.cpp
	g++ -c frog.cpp
lilipad.o: lilipad.cpp
	g++ -c lilipad.cpp
object.o: object.cpp
	g++ -c object.cpp
saveFrog.o: saveFrog.cpp
	g++ -c saveFrog.cpp
typeFonts.o: typeFonts.cpp
	g++ -c typeFonts.cpp
clean:
	rm -f *.o main
