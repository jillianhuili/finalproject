//
//  log.cpp
//
//
//  Created by Huili Chen on 3/25/13.
//  
//

#include "SDL/SDL.h"
#include "log.h"
#include "object.h"
#include <deque>
#include <algorithm>
#include <iterator>
#include <iostream>
using namespace std;

//constructor
log::log(SDL_Surface* Screen):obj(Screen)
{
	//create temporary deque
        deque<SDL_Surface*> images;

	//load images 
	SDL_Surface* image1=obj.load_image("longlog.png");
	SDL_Surface* image2=obj.load_image("shortlog.png");
	
	//add images to deque	
	images.push_back(image1);
	images.push_back(image2);
	
	//add random image to log_image
	int i=rand()%images.size();
   	log_image=images[i];
    
	//set location
    	location.x=100;
    	location.y=100;
	location.h=50;
	if(i==1){
		location.w=200;
	}else{
		location.w=300;
	}    
}

//draw a log on the screen.
void log::put_image_on_screen()
{
    
    obj.print(location.x,location.y,log_image);    
}

//free surface
void log::free_surface()
{ 
    SDL_FreeSurface(log_image);   
}

//set positions
void log::setPosition(int xx, int yy)
{
    location.x=xx;
    location.y=yy;  
}

//set velocity
void log::setVelocity(int x)
{
	x_speed=x;
}

//returns width
int log::getWidth()
{
	return location.w;
}

//returns color
string log::get_color()
{
    	return color;
}

//returns x position
int log::get_x_position()
{
    	return location.x;    
}

//returns y position
int log::get_y_position()
{
    	return location.y;    
}

//returns location in form of SDL_Rect
SDL_Rect log::getLocation()
{
	return location;
}
