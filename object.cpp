/*
object.cpp
contains the print and load picture functions
by: Jillian Montalvo
*/

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "object.h"
#include <iostream>
using namespace std;

//constructor
object::object(SDL_Surface* iscreen)
{
	screen = iscreen;
}

//print function
void object::print(double x, double y, SDL_Surface* picture)
{
	//Make a temporary rectangle to hold the offsets
	SDL_Rect offset;

	//Give the offsets to the rectangle
	offset.x = x;
	offset.y = y;

	//Blit the surface
	SDL_BlitSurface(picture, NULL, screen, &offset);
}

//loads image
SDL_Surface* object::load_image(string filename)
{
	//Temporary storage for the image that's loaded
	SDL_Surface* loadedImage = NULL;

	//The optimized image that will be used
	SDL_Surface* optimizedImage = NULL;

	//Load the image
	loadedImage = IMG_Load(filename.c_str());

	//If nothing went wrong in loading the image
	if(loadedImage != NULL){
		//Create an optimized image
		optimizedImage = SDL_DisplayFormat(loadedImage);
		//Free the old image
		SDL_FreeSurface(loadedImage);

		//If the image was optimized just fine
		if(optimizedImage!=NULL){
			//set white to be invisible
			//Map the color key
			Uint32 colorkey = SDL_MapRGB(optimizedImage->format,0xFF,0xFF,0xFF);
			//Set all pixels of color R 0, G 0xFF, B 0xFF to be transparent
			SDL_SetColorKey(optimizedImage, SDL_SRCCOLORKEY, colorkey);
		}
	}

	//Return optimized image
	return optimizedImage;
}
